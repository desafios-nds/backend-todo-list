﻿using IFCE.TodoList.Domain.Model;
using IFCE.TodoList.Domain.Contracts;

namespace IFCE.TodoList.Infra.Data.Paged;

public class PagedResult<T> : IPagedResult<T> where T : Entity, new()
{
    public PagedResult()
    {
        Items = new List<T>();
    }
    
    public List<T> Items { get; set; }
    public int Total { get; set; }
    public int Page { get; set; }
    public int PerPage { get; set; }
    public int PageCount { get; set; }
}