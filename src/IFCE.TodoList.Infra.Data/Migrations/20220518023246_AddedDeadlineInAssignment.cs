﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IFCE.TodoList.Infra.Data.Migrations
{
    public partial class AddedDeadlineInAssignment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Deadline",
                table: "Assignments",
                type: "DATETIME",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Deadline",
                table: "Assignments");
        }
    }
}
