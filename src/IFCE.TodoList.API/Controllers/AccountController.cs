﻿using IFCE.TodoList.Application.Notifications;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Annotations;

namespace IFCE.TodoList.API.Controllers;

[Authorize]
[SwaggerTag("Account")]
public class AccountController : MainController
{
    public AccountController(INotificator notificator) : base(notificator)
    {
    }
}