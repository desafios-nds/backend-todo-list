﻿using System.ComponentModel.DataAnnotations;

namespace IFCE.TodoList.Application.DTO.Assignment;

public class EditAssignmentDto
{
    [Required]
    public Guid Id { get; set; }
    [Required]
    public string Description { get; set; }
    public DateTime? Deadline { get; set; }
    public Guid? AssignmentListId { get; set; }
    public bool Concluded { get; set; }
    public DateTime? ConcludedAt { get; set; }
    public Guid UserId { get; set; }
}