﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Bogus;
using FluentAssertions;
using FluentAssertions.Execution;
using IFCE.TodoList.Application.DTO.Assignment;
using IFCE.TodoList.Application.Notifications;
using IFCE.TodoList.Application.Services;
using IFCE.TodoList.Domain.Contracts.Repository;
using IFCE.TodoList.Domain.Model;
using Microsoft.AspNetCore.Http;
using Moq;
using Xunit;

namespace IFCE.TodoList.Application.Test.Services;

public class AssignmentServiceTest
{
    private readonly Guid _userId = Guid.NewGuid();
    private readonly Guid _assignmentId = Guid.NewGuid();
    private readonly AssignmentService _assignmentService;

    private readonly Mock<INotificator> _notificatorMock = new();
    private readonly Mock<IAssignmentRepository> _assignmentRepositoryMock = new();
    private readonly Mock<IHttpContextAccessor> _httpContextAccessorMock = new();

    private readonly Faker<Assignment> _assignmentFaker;
    
    public AssignmentServiceTest()
    {
        _assignmentFaker = CreateFaker();
        
        _assignmentService =
            new AssignmentService(CreateMapper(), _assignmentRepositoryMock.Object, _httpContextAccessorMock.Object, 
                _notificatorMock.Object, null);
    }
    
    [Fact]
    public async Task GetById_ExistentId_ReturnAssignment()
    {
        // Arrange
        //ConfigureMocks(_userId);
        
        // Act
        //var assignment = await _assignmentService.GetById(Guid.NewGuid());

        // Assert
        using (new AssertionScope())
        {
            //assignment.Should().NotBeNull();
            //_notificatorMock.Object.IsNotFoundResource.Should().BeFalse();
            //_notificatorMock
            //    .Verify(c => c.HandleNotFoundResource(), Times.Never);
        }
    }
    
    private void ConfigureMocks(Guid userId, bool claimNull = false)
    {
        _assignmentRepositoryMock
            .Setup(c 
                => c.GetById(It.Is<Guid>(t => t == _assignmentId), It.Is<Guid>(t => t == _userId)))
            .ReturnsAsync(_assignmentFaker.Generate());
        
        _assignmentRepositoryMock
            .Setup(c 
                => c.GetById(It.Is<Guid>(t => t != _assignmentId), It.Is<Guid>(t => t != _userId)))
            .ReturnsAsync(null as Assignment);

        _notificatorMock
            .Setup(c => c.HandleNotFoundResource());
        
        _httpContextAccessorMock
            .Setup(c => c.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Id"))
            .Returns(claimNull ? null : new Claim("Id", userId.ToString()));
    }

    private static IMapper CreateMapper() => 
        new MapperConfiguration(mc =>
        {
            mc.CreateMap<AssignmentDto, Assignment>().ReverseMap();
            mc.CreateMap<AddAssignmentDto, Assignment>().ReverseMap();
            mc.CreateMap<EditAssignmentDto, Assignment>().ReverseMap();
        }).CreateMapper();
    
    private static Faker<Assignment> CreateFaker()
    {
        var faker = new Faker<Assignment>();

        faker
            .RuleFor(c => c.Description, f => f.Lorem.Text())
            .RuleFor(c => c.Concluded, f => f.Random.Bool());
        
        return faker;
    }
}